//Andrea Garcia Arranz y Adriana Ventura Candela 1�G 


#include "jugLista.h"

#ifndef LISTAEDIFICIOS_H
#define LISTAEDIFICIOS_H
#include <string>
#include <iostream>

using namespace std;

const int LISTA_EDIFICIOS_MAX = 50;

typedef enum {disponible,comprado, borrado}  tEstado;

typedef struct
{
    int codigo;
    string nombre;
    int precio;
    int dinero;
    double prestigio;
    tEstado estadoEdificio;
} tEdificio;

typedef struct
{
    tEdificio listaEdif[LISTA_EDIFICIOS_MAX];
    int contador;
} tListaEdificios;

//funcion que recibe como parametro una lista de edificios y devuelve un booleano indicando si la lista esta llena o no
bool listaEdificiosLlena(const tListaEdificios &listaEdif);

/*Recibe una lista de edificios y un c�digo. Indica si se ha encontrado o no un edificio con ese c�digo y, en caso afirmativo, la posici�n en
la que se encuentra. Debe estar implementado como una b�squeda binaria. */
int buscarEdificio(tListaEdificios &listaEdif ,int codigo);

/* Funcion auxiliar */
int buscarEdificio(tListaEdificios &listaEdif,int codigo,int a,int b);

/* Recibe un tEdificio y una lista de edificios. Inserta el edificio en la lista en la posici�n que le corresponda por c�digo y devuelve la
lista actualizada.*/
tListaEdificios insertarEdificio(tEdificio &edif, tListaEdificios &listaEdif);

/*Recibe la lista edificios y un c�digo de edificio. Si el edificio existe y est� disponible, lo marca como borrado en la lista.*/
bool bajaEdificio(tListaEdificios &listaEdif,int codigo);

/* Lista todos los edificios de nuestra lista debidamente formateados */
void listadoEdificios(tListaEdificios &listaEdif);

/*Dado un tEdificio muestra todos sus datos en la consola debidamente formateados*/
void mostrarEdificio(tEdificio &edif);

char* deEstadoEdificioAString(tEstado e);

tEstado deValorAEstadoEdificio(int valor);


/*creamos un nuevo edificio*/
tEdificio nuevoEdificio();

#endif


