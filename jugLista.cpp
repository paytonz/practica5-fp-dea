//Andrea Garcia Arranz y Adriana Ventura Candela 1ºG 


#include "jugLista.h"
#include <iostream>
#include <string>
using namespace std;

bool listaJugadoresLlena(const tListaJugadores &listaJug, bool &llena)
{
    if(listaJug.contador == LISTA_JUGADORES_MAX) llena = true;
    else llena = false;
    return llena;
}

void buscarJugador(const tListaJugadores &listaJug, string nombre, bool &encontrado, int &posicion)
{
    posicion  = -1;
    encontrado = false;

    for (int i = 0; i < listaJug.contador; i++)

        if (listaJug.jugadores[i].nombre == nombre)
        {
            encontrado = true;
            posicion = i;
        }
}

bool insertaJugador(tJugador nuevoJugador, tListaJugadores &listaJug, bool &insertaJug)
{
    bool encontrado;
    int posicion;

    if(listaJug.contador == LISTA_JUGADORES_MAX)
    {
        insertaJug = false;
    }
    else
    {
        buscarJugador(listaJug, nuevoJugador.nombre, encontrado, posicion);
        if(encontrado == true)
        {
            insertaJug = false;
        }
        else
        {
            for(int i = listaJug.contador; i > posicion; i--)
            {
                listaJug.jugadores[i] = listaJug.jugadores[i-1];
            }
            listaJug.jugadores[posicion] = nuevoJugador;
            listaJug.contador++;
            insertaJug = true;
        }
    }
    return insertaJug;
}

//nombre, dinero y prestigio por turno
void mostrarJugador(const tJugador jugador, tListaEdificios &listaEdif, bool mostrarInfo)
{
    cout << "Nombre: " << jugador.nombre << endl;
    cout << "Dinero: " << jugador.dinero << endl;
    cout << "Prestigio: " << jugador.prestigio << endl;

    if(mostrarInfo == true)
    {
        cout  << "El jugador " << jugador.nombre << " tiene " << listaEdif.contador << " edificios" << endl;
    }
    cout << endl;
}

void listadoJugadores(tListaJugadores &listaJug, tListaEdificios &listaEdif)
{

    for(int i=0; i < listaJug.contador; i++  )
    {
        mostrarJugador(listaJug.jugadores[i],listaEdif, true);
    }
}

//Pedir datos del jugador previo haberlos inicializado (comprobar array)
tJugador nuevoJugador()
{

    tJugador jug;
    tListaEdificios listaEdif;

    string contrasenya, nombre, universidad;

    jug.contrasenya = contrasenya;
    jug.dinero = 3000;
    jug.nombre = nombre;
    jug.prestigio = 0;
    jug.universidad = universidad;

    return jug;
}

bool bajaJugador(string nombre, tListaJugadores &listaJug)
{
    bool encontrado = true;
    int lugar;
    buscarJugador(listaJug, nombre,encontrado,lugar);
    if(encontrado == true)
    {
        for(int i= lugar; i < listaJug.contador - 1; i++)
        {
            listaJug.jugadores[i] = listaJug.jugadores[i+1];
        }
        listaJug.contador--;
    }
    return encontrado;
}

//Recibe como entrada un tJugador y devuelve un booleano indicando si su lista de edificios comprados esta llena
bool listaCompradosLlena(const tJugador &jug)
{

    bool llena = true;

    for (int i = 0; i < jug.comprados.contador; i++)////////////////////
        if (jug.comprados.listaEdif[i].codigo == 0)
            return !llena;

    return llena;
}

bool comprarEdificio(tJugador &jug, int codigo, tListaEdificios &listaEdif)
{

    int pos = buscarEdificio(listaEdif, codigo);

    if (pos != -1)
    {

        if (listaEdif.listaEdif[pos].estadoEdificio == disponible && jug.dinero >= listaEdif.listaEdif[pos].dinero) //Dos pisos
        {

            insertarEdificio(listaEdif.listaEdif[pos], jug.comprados);
            bajaEdificio(listaEdif, codigo);
            jug.dinero = jug.dinero - listaEdif.listaEdif[pos].dinero;
            cout << "El edificio se compro satisfactoriamente";
        }
        else
            cout << "El jugador no dispone de dinero o el edificio no esta disponible" << endl;
    }
    else
        cout << "No se puede encontrar el edificio";

    return true;
}

/*bool comprarEdificio(tJugador jugador, int codigoEd, tListaEdificios listaEdificios){
	tEdificio edificio;
	bool comprado = false;
	bool error = false;
	bool existeEd;
	edificio.codigo = codigoEd;
	existeEd = buscarEdificio (listaEdificios, edificio.codigo);
	if (!existeEd){
		error = true;
		cout << " El edificio no existe";
	}
	if (edificio.estadoEdificio != disponible){
		error = true;
		cout << " El edificio no esta disponible" << endl;
	}
	if (jugador.dinero < edificio.precio){
		error = true;
		cout << " No hay suficiente dinero como para efectuar la compra" << endl;
	if (!error){
		edificio.estadoEdificio = comprado;
		jugador.dinero= jugador.dinero - edificio.precio;
		int i=0;
		while (jugador.comprados[i]!=-1){
			if (jugador.comprados[i]==-1){
				jugador.comprados[i]=codigoEd;
				jugador.comprados[i+1]=-1;
			}
			i++;
		}
		comprado=true;
	}
	return comprado;
}*/
