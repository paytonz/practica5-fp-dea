//Andrea Garcia Arranz y Adriana Ventura Candela 1ºG 



#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "listaEdificios.h"
#include "jugLista.h"

void eleccionMenu(tListaJugadores &listaJug, tListaEdificios &listaEdif);

void inicializar(tListaJugadores &listaJug, tListaEdificios &listaEdif);

void leerArchivoJug(tListaJugadores &listaJug);
void guardarArchivoJug(const tListaJugadores &listaJug);
void cargaFichero(tListaEdificios &listaEdif);
void guardarEnFichero(tListaEdificios &listaEdif);
void borrarJugador(tListaJugadores &listaJug);
void altaNuevoJugador(tListaJugadores &listaJug);

void verMisEdificios(tJugador &jug);
void verMisEdificiosDisponibles(tListaEdificios &listaEdif);
void verClasificacionDinero(tListaJugadores &listajug);
void verClasificacionPrestigio(tListaJugadores &listajug);

void passwAdmin(string password, bool &ok);
void menuAdministracion(int &op);
void menuJuego(int &op);
void opcAdmin();
void opcJuego(tJugador &jug, tListaJugadores &listajug, tListaEdificios &listaEdif);

int main()
{

    string nombre;
    tListaJugadores listaJug;
    tListaEdificios listaEdif;
    /*
    	cout << "Pagina de codigos activa: " << sizeof(tListaJugadores) << endl;
    	cout << "Pagina de codigos activa: " << sizeof(tListaEdificios) << endl << endl;
    */
    leerArchivoJug(listaJug);
    cargaFichero(listaEdif);

    eleccionMenu(listaJug, listaEdif);

    cin.sync();
    cin.get();
    return 0;
}

void eleccionMenu(tListaJugadores &listaJug, tListaEdificios &listaEdif)
{

    bool encontrado;
    int posicion;
    bool salir = false;
    string nombre;

    cout << "\n";
    cout << "BIENVENIDO A CAMPUS VILLE" << endl;
    cout << "--------------------------- " << endl << endl;

    cout << "Escribe 'salir' o 's' para cerrar el programa..." << endl;//pide al usuario confirmacion para seguir jugando
    do
    {
        cout << "Usuario: ";
        getline(cin, nombre);
        if(nombre == "s" || nombre == "salir")salir = true;
        else
        {
            if(salir == false)
            {

                buscarJugador(listaJug,nombre,encontrado,posicion);

                if(encontrado == true)
                {
                    opcJuego(listaJug.jugadores[posicion], listaJug, listaEdif);
                }
                else
                {
                    opcAdmin();
                }

            }
        }
    }
    while(salir==false);
}

void opcAdmin()
{
    string password;
    string nombre;
    bool ok;
    int op, codigo;
    tListaEdificios lista;
    tListaJugadores listaJug;

    passwAdmin(password, ok);
    if(ok == false)
    {
    }
    else
    {
        menuAdministracion(op);
        do
        {
            if(op == 1) listadoEdificios(lista);
            else if(op == 2)
            {
                nuevoEdificio();
                //LLAMAMOS A LA FUNCION GUARDAR EN FICHERO
                guardarEnFichero(lista);
            }
            else if(op == 3)
            {
                cout << "Introduce un codigo: " << endl;
                cin >> codigo;
                if (bajaEdificio(lista, codigo)==false)
                {
                    cout << "codigo no encontrado" <<endl;
                }
                else
                {
                    cout << "Borrado satisfactorio" << endl;
                }
            }
            else if(op == 4) listadoJugadores(listaJug, lista);
            else if(op == 5) altaNuevoJugador(listaJug);
            else if(op == 6) borrarJugador(listaJug);
            else if(op == 7) ;
            else if(op == 8);
            else if(op == 0);
        }
        while(op != 0);
    }
}

void opcJuego(tJugador &jug, tListaJugadores &listajug, tListaEdificios &listaEdif)
{
    int op, codigo;
    string contrasenya;

    cout << "Password: ";
    getline(cin, contrasenya);
    menuJuego(op);
    do
    {
        if(op == 1) verMisEdificios(jug);
        else if(op == 2) verMisEdificiosDisponibles(listaEdif);
        else if(op == 3)
        {
            cout << "Introduce codigo del edificio que quieres comprar:" ;
            cin >> codigo;

            comprarEdificio(jug, codigo, listaEdif);
        }
        else if(op == 4) verClasificacionDinero(listajug);
        else if(op == 5) verClasificacionPrestigio(listajug);
        else if(op == 0);

        menuJuego(op);
    }
    while(op != 0);
}

void verMisEdificios(tJugador &jug)
{

    for (int i = 0; i < jug.comprados.contador; i++)
    {

        mostrarEdificio(jug.comprados.listaEdif[i]);
        cout << endl;
    }
}

void verMisEdificiosDisponibles(tListaEdificios &listaEdif)
{

    for (int i = 0; i < listaEdif.contador; i++)
    {

        mostrarEdificio(listaEdif.listaEdif[i]);
        cout << endl;
    }
}

void verClasificacionDinero(tListaJugadores &listajug)
{


}

void verClasificacionPrestigio(tListaJugadores &listajug)
{


}

void passwAdmin(string password, bool &ok)
{

    int cont = 0;
    do
    {
        cout << "Password < " << 3-cont << " intentos> : ";
        cin >> password;
        if(password == "12345") ok = true;
        else
        {
            cont++;
            if(cont < 3) ok = false;
            else ok = true;
        }
    }
    while(ok == false);

}

void menuAdministracion(int &op)
{

    do
    {
        cout << endl;
        cout << "--- MENU ADMINISTRACION ---" << endl;
        cout << "1. Ver el listado de edificios" << endl;
        cout << "2. Nuevo edificio" << endl;
        cout << "3. Borrar un edificio" << endl;
        cout << "4. Ver el listado de jugadores" << endl;
        cout << "5. Nuevo jugador" << endl;
        cout << "6. Borrar un jugador" << endl;
        cout << "7. Ejecutar un turno" << endl;
        cout << "8. Ver la clasificacion" << endl;
        cout << "0. Cerrar la sesion" << endl;
        cout << endl;

        cin.sync();
        cout << "Elija una opcion: ";
        cin >> op;
        cout << endl;

        cin.sync();

    }
    while(op < 0 || op > 8);

}

void menuJuego(int &op)
{

    do
    {
        cout << endl;
        cout << "--- MENU DE JUEGO ---" << endl;
        cout << "1. Ver mis edificios" << endl;
        cout << "2. Ver los edificios disponibles" << endl;
        cout << "3. Comprar un edificio" << endl;
        cout << "4. Ver la clasificacion (ordenada por dinero)" << endl;
        cout << "5. Ver la clasificacion (ordenada por prestigio)" << endl;
        cout << "0. Cerrar la sesion" << endl;
        cout << endl;

        cin.sync();
        cout << "Elija una opcion: ";
        cin >> op;
        cout << endl;

        cin.sync();

    }
    while(op < 0 || op > 5);

}

void leerArchivoJug(tListaJugadores &listaJug)
{

    ifstream archivo;
    archivo.open("jugadores.txt");
    string nombre;
    bool fin = false;

    int cont = 0;
    int cont2 = 0;
    int codigo;
    string limpiar;

    listaJug.contador = 0;

    if(!archivo.is_open()) cout << "El archivo 'jugadores.txt' no existe. El programa empezara con una lista vacia" << endl;
    else
    {

        while(cont < LISTA_JUGADORES_MAX && !fin)
        {
            archivo >> nombre;

            if (nombre.compare("x") != 0 && nombre.compare("X") != 0)
            {

                listaJug.jugadores[cont].nombre = nombre;
                archivo >> listaJug.jugadores[cont].contrasenya;
                getline(archivo, limpiar);
                getline(archivo, listaJug.jugadores[cont].universidad);
                archivo >> listaJug.jugadores[cont].dinero;
                archivo >> listaJug.jugadores[cont].prestigio;
                archivo >> codigo;

                while (codigo != -1 && cont2 < LISTA_EDIFICIOS_MAX)
                {
                    listaJug.jugadores[cont].comprados.listaEdif[cont2].codigo = codigo;
                    archivo >> codigo;
                    cont2++;
                }

                cont2 = 0;
            }
            else
                fin = true;

            cont++;
            listaJug.contador++;
        }
    }

    archivo.close();
}

void cargaFichero(tListaEdificios &listaEdif)
{

    tEdificio aux;
    int aux_estado;
    ifstream ed;
    string limpiar;

    ed.open("edificios.txt");
    if (ed.is_open())
    {

        listaEdif.contador=0;

        ed >> aux.codigo;

        while(aux.codigo!=-1)
        {

            getline(ed, limpiar);
            getline(ed, aux.nombre);
            ed >> aux.precio;
            ed >> aux.dinero;
            ed >> aux.prestigio;
            ed >> aux_estado;
            aux.estadoEdificio = deValorAEstadoEdificio(aux_estado);
            listaEdif.listaEdif[listaEdif.contador] = aux;
            listaEdif.contador++;
            ed >> aux.codigo;
        }
        cout << "Se insertaron " << listaEdif.contador << " edificios con exito!" << endl;
    }
    else
    {
        cout << "Archivo edificios.txt no encontrado!" << endl << endl;
    }
}

void guardarArchivoJug(const tListaJugadores &listaJug)
{

    ofstream archivo;
    archivo.open("jugadores.txt");

    if(archivo.is_open())
    {
        for(int i =0; i < listaJug.contador; i++)
        {
            archivo << listaJug.jugadores[i].nombre << endl;
            archivo << listaJug.jugadores[i].contrasenya << endl;
            archivo << listaJug.jugadores[i].universidad << endl;
            archivo << listaJug.jugadores[i].dinero << endl;
            archivo << listaJug.jugadores[i].prestigio << endl;
            archivo << listaJug.jugadores[i].comprados.listaEdif[i].codigo << endl;
        }

        cout << "Archivo guardado" << endl;
        cout << "Jugadores guardados" << endl;
    }
    else
    {
        cout << "Error de apertura" << endl;
    }

    archivo << -1 << endl;
    archivo << "X" << endl;
    archivo.close();
}

void guardarEnFichero(tListaEdificios &listaEdif)
{

    tEdificio nuevo_edificio;
    //int nuevo_edificio_estado;
    ofstream ed;
    //string limpiar;

    ed.open("edificios.txt");
    if (!ed.is_open())
    {
        cout << "Archivo edificios.txt no encontrado!" << endl;
    }
    else
    {
        ed << nuevo_edificio.codigo;
        ed << nuevo_edificio.nombre;
        ed << nuevo_edificio.precio;
        ed << nuevo_edificio.dinero;
        ed << nuevo_edificio.prestigio;
        ed << nuevo_edificio.estadoEdificio;
    }
}

void borrarJugador(tListaJugadores &listaJug)
{
    string nombre;
    bool borrar;

    cout << "Introduce el nombre del jugador a dar de baja: ";
    getline(cin, nombre);

    borrar = bajaJugador(nombre,listaJug);
    if(borrar == true) cout << "Jugador eliminado" << endl;
    else cout << "ERROR.No se ha encontrado el jugador" << endl;
}

void altaNuevoJugador(tListaJugadores &listaJug)
{
    tJugador jugador;
    bool llena = false, insertar;

    llena = listaJugadoresLlena(listaJug,llena);
    if(llena == true)
    {
        cout << "ERROR. La lista de jugadores esta llena" << endl;
    }
    else
    {
        nuevoJugador();
        insertar = insertaJugador(jugador,listaJug,insertar);

        if(insertar == true) cout << "Jugador registrado" << endl;
        else cout << "ERROR. El jugador ya existia" << endl;

    }
}
