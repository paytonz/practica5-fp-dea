//Andrea Garcia Arranz y Adriana Ventura Candela 1�G 

#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>

#include "listaEdificios.h"
//vale
using namespace std;

//lista de edificios
bool bajaEdificio(tListaEdificios &listaEdificios,int codigo)
{

    //pide al admin un codigo y lo procesa

    int pos = buscarEdificio(listaEdificios,codigo);

    //si ese codigo existe entre los codigos del fichero edificios
    //lo borra (se marca como borrado) el edificio se borrara si no se ha comprado por nadie

    //busca la posicion del edificio y la marca como borrada
    if ( pos != -1 )
    {


        if (listaEdificios.listaEdif[pos].estadoEdificio == comprado)
        {

            listaEdificios.listaEdif[pos].estadoEdificio = borrado;

        }
        else
        {
            if (pos==listaEdificios.contador-1)
            {
                listaEdificios.contador--;
            }

            for (int i = pos; i < listaEdificios.contador - 2; i++)
            {
                listaEdificios.listaEdif[pos] = listaEdificios.listaEdif[pos+1];

            }

        }
        return true;
    }
    /*comprobamos que el edificio no esta comprado
    **si esta comprado -> marcamos como borrado
    **si no lo esta -> procedemos a borrar.
    */
    return false;
}

/* Recibe un tEdificio y una lista de edificios. Inserta el edificio en la lista en la posici�n que le corresponda por c�digo y devuelve la
lista actualizada.*/
tListaEdificios insertarEdificio(tEdificio &edif, tListaEdificios &listaEdif)
{


	int pos = buscarEdificio(listaEdif, edif.codigo);

    if(pos==-1) pos =0;

   	for (int i = listaEdif.contador-1; i >= pos ; i -- )////////////////////
			listaEdif.listaEdif[i+1] = listaEdif.listaEdif[i];





		listaEdif.listaEdif[pos] = edif;
		listaEdif.contador++;



	return listaEdif;
}


int buscarEdificio(tListaEdificios &listaEdif,int codigo)
{
    int total= listaEdif.contador;
    int mitad = total /2 ;


    if (listaEdif.listaEdif[mitad].codigo==codigo) return mitad;
    else if(listaEdif.listaEdif[mitad].codigo<codigo) return buscarEdificio(listaEdif,codigo,mitad+1,total);
    else return buscarEdificio(listaEdif,codigo,0,mitad);

}

// Implementacion recursiva
int buscarEdificio(tListaEdificios &listaEdif,int codigo,int a , int b)
{
    int mitad = (b+a) /2 ;

    if (b>a)
    {
        if (listaEdif.listaEdif[mitad].codigo==codigo) return mitad;
        else if(listaEdif.listaEdif[mitad].codigo<codigo) return buscarEdificio(listaEdif,codigo,mitad+1,b);
        else return buscarEdificio(listaEdif,codigo,a,mitad);
    }
    return -1;
}

bool listaEdificiosoLlena(tListaEdificios &listaEdif)
{

    bool listaCompleta = false;

    if (listaEdif.contador == LISTA_EDIFICIOS_MAX)
    {

        cout << "lista llena!";
        listaCompleta = true;
    }
    return listaCompleta;
}

void listadoEdificios(tListaEdificios &listaEdif)
{
    for(int i=0; i < listaEdif.contador; i++  )
    {
        cout << "================================================" << endl;
        mostrarEdificio(listaEdif.listaEdif[i]);

    }
}

//Edificio
void mostrarEdificio(tEdificio &edificio)
{
    cout << "\t Identificador: " << edificio.codigo <<endl;
    cout << "\t Nombre: " <<  edificio.nombre  <<endl;;
    cout << "\t Precio de compra: "<<   edificio.precio  <<endl;;
    cout << "\t Dinero por turno : " <<  edificio.dinero  <<endl;;
    cout << "\t Prestigio por turno : "  <<  edificio.prestigio   <<endl;;
    cout <<  "\t Estado: "<<  deEstadoEdificioAString(edificio.estadoEdificio)  <<endl;

}
char* deEstadoEdificioAString(tEstado e)
{

    char * resultado= new char[50];

    if (e==0) strcpy(resultado,"DISPONIBLE");
    else if (e==1) strcpy (resultado,"COMPRADO");
    else if (e==2) strcpy (resultado,"BORRADO");

    return resultado;
}

tEstado deValorAEstadoEdificio(int valor)
{

    tEstado nuevo_estado;

    if(valor == 0) nuevo_estado=disponible;
    else if(valor == 1) nuevo_estado=comprado;
    else if(valor == 2) nuevo_estado=borrado;

    return nuevo_estado;
}

//Pedir datos del edificio previo haberlos inicializado
tEdificio nuevoEdificio()
{

    tEdificio edif;
    int codigo = 0, dinero = 0, precio = 0, prestigio = 0, estado = 0;
    string nombre;

    cout << "Codigo:";
    cin >> codigo;
    cout << "Dinero:";
    cin >> dinero;
    cout << "Nombre";
    cin >> nombre;
    cout << "Precio:";
    cin >> precio;
    cout << "Prestigio:";
    cin >> prestigio;

    edif.codigo = codigo;
    edif.dinero = dinero;
    edif.estadoEdificio = deValorAEstadoEdificio(estado);
    edif.nombre = nombre;
    edif.precio = precio;
    edif.prestigio = prestigio;

    return edif;
}

/* Recibe un tEdificio y una lista de edificios. Inserta el edificio en la lista en la posici�n que le corresponda por c�digo y devuelve la
lista actualizada.*/
