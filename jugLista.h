//Andrea Garcia Arranz y Adriana Ventura Candela 1�G 


#ifndef JUGLISTA_H
#define JUGLISTA_H

#include <string.h>

#include "listaEdificios.h"

using namespace std;

const int LISTA_JUGADORES_MAX = 20; //numero maximo de jugadores

//vale
typedef struct
{
    string nombre;
    string contrasenya;
    string universidad;
    int dinero;
    int prestigio;
    tListaEdificios comprados;
} tJugador;

typedef struct
{
    tJugador jugadores[LISTA_JUGADORES_MAX];
    int contador;
} tListaJugadores;


/*Lee los datos de un jugador, inicializa su lista de edificios comprados como vacia, el dinero a 3000 creditos y los puntos de prestigio a 0
Devuelve la estructura tJugador debidamente inicializada*/
tJugador nuevoJugador();

/*Dado un tJugador y la lista de edificios, muestra en la consola los datos del jugador y los datos basicos de todos sus edificios
(nombre, dinero y prestigio por turno)*/
void mostrarJugador(const tJugador jugador, tListaEdificios &listaEdif, bool mostrarInfo);

//Recibe como entrada un tJugador y devuelve un booleano indicando si su lista de edificios comprados esta llena
bool listaCompradosLlena(const tJugador &jug, bool &llena);

/*Recibe como entrada un tJugador, un codigo de edificio y la lista completa de edificios. Si se puede hacer la compra (el edicio existe,
esta disponible y hay dinero suficiente) realiza la compra y a�ade el codigo de edificio a la lista de edificios del jugador
*/
bool comprarEdificio(tJugador &jug, int codigo, tListaEdificios &listaEdif);

//funcion que recibe como parametro una lista de jugadores y devuelve un booleano indicando si la lista esta llena o no
bool listaJugadoresLlena(const tListaJugadores &listaJug, bool &llena);

/*Funcion que rebibe como parametro de entrada una lista de jugadores y un nombre de usuario. Devuelve un booleano indicando si se ha encontrado
el jugador o no y, en caso afirmativo, la posicion en la que se encuentra el jugador*/
void buscarJugador(const tListaJugadores &listaJug, string nombre, bool &encontrado, int &posicion);

//Recibe una variable de un tipo tJugador y una lista de jugadores. Inserta el jugador al final de la lista de jugadores
bool insertaJugador(tJugador nuevoJugador, tListaJugadores &listaJug, bool &insertaJug);

//Recibe un nombre de usuario, la lista de jugadores. Busca al jugador en la lista y lo elimina completamente
bool bajaJugador(string nombre, tListaJugadores &listaJug);

//muestra una lista de jugadores
void listadoJugadores(tListaJugadores &listaJug, tListaEdificios &listaEdif);
//bool comprarEdificio(tJugador jugador, int codigoEd, tListaEdificios listaEdificios);

#endif
